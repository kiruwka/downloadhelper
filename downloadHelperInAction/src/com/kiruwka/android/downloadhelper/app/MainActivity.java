package com.kiruwka.android.downloadhelper.app;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.kiruwka.android.download.DownloadHelper;
import com.kiruwka.android.download.DownloadHelper.OnCompleteListener;
import com.kiruwka.android.download.DownloadHelper.OnErrorListener;
import com.kiruwka.android.download.DownloadHelper.OnProgressListener;
import com.kiruwka.android.downloadhelper.app.SimpleListAdapter.ViewHolder;

public class MainActivity extends ListActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    // keeps track of list items data(progress, state)
    private SimpleListAdapter mAdapter = null;
    private DownloadHelper mDownloadHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDownloadHelper = DownloadHelper.getInstance(this);
        mAdapter = new SimpleListAdapter(this);
        setListAdapter(mAdapter);
        // setDefaultUncaughtExceptionHandler(); // for debugging
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDownloadHelper.unregisterAllListeners();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDownloadHelper.registerOnErrorListener(mOnErrorListener);
        mDownloadHelper.registerOnProgressListener(mOnProgressListener);
        mDownloadHelper.registerOnCompleteListener(mOnCompleteListener);
        // this is not really cool doing it in UI thread, just a hack to make sure we refresh progresses correctly
        mAdapter.initStates();
        mAdapter.notifyDataSetChanged();
    }

    // download event listeners
    private OnErrorListener mOnErrorListener = new OnErrorListener() {
        @Override
        public void onError(long uid, String msg) {
            toastMsg("ERROR downloding " + mAdapter.getNameByUid(uid) + "!");
        }
    };

    private OnProgressListener mOnProgressListener = new OnProgressListener() {
        @Override
        public void onProgressUpdate(long uid, int progress) {
            updateProgress(uid, progress);
        }
    };

    private OnCompleteListener mOnCompleteListener = new OnCompleteListener() {
        @Override
        public void onComplete(long uid) {
            String filePath = SimpleListAdapter.mDirLocation + mAdapter.getNameByUid(uid);
            toastMsg(String.format(filePath + "%n downloaded!"));
            updateProgress(uid, 100);
        }
    };

    // private helper methods

    private void updateProgress(long uid, int progress) {
        Log.d(TAG, "progress for " + uid + "= " + progress);
        int position = mAdapter.getPosByUid(uid);
        mAdapter.setProgressData(position, progress);
        ListView listView = getListView();

        int first = listView.getFirstVisiblePosition();
        int last = listView.getLastVisiblePosition();
        if (position < first || position > last) {
            return; // view not visible
        }
        View view = listView.getChildAt(position - first);
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.progress.setProgress(progress);
        if (progress == 100) { // downloaded, show clickabe "play" overlay
            holder.play.setVisibility(View.VISIBLE);
        }
    }

    // helper method to show toast on UI thread
    private void toastMsg(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * debug helper to make uncaught exceptions show error instead of crashing our threads and app
     */
    private void setDefaultUncaughtExceptionHandler() {
        try {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread t, Throwable e) {
                    Log.e(TAG, "Uncaught Exception detected in thread " + t.getName() + " : " + e);
                    // FIXME(KM): only for debug purpose
                    toastMsg(String.format("Error : %n" + e.getMessage()));
                }
            });
        } catch (SecurityException e) {
            Log.e(TAG, "Could not set the Default Uncaught Exception Handler");
        }
    }
}
