package com.kiruwka.android.downloadhelper.app;

import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.kiruwka.android.download.DownloadHelper;
import com.kiruwka.android.download.DownloadHelper.DownloadStatus;

public class SimpleListAdapter extends BaseAdapter {
    private static final String TAG = SimpleListAdapter.class.getSimpleName();
    public static String mDirLocation = Environment.getExternalStorageDirectory().toString()
            // Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
            .toString()
            + File.separator + "shutterstock"
            + File.separator;

    private Context mContext = null;
    private LayoutInflater mInflater = null;
    private DownloadHelper mDh = null;
    // dataSet representing state for each displayed item
    private State[] mStates = new State[NUM_ITEMS];
    private Bitmap[] mBitmaps = new Bitmap[NUM_ITEMS];

    public SimpleListAdapter(Context context) {
        mContext = context;
        mDh = DownloadHelper.getInstance(context);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // normally this shouldn't be here, but this is for testing only with small lists with small thumbnails
        initBitmaps(); // just a hack for scrolling speedup
        initStates();
    }

    public void initStates() {
        for (int pos = 0; pos < NUM_ITEMS; ++pos) {
            long uid = mUids[pos];
            DownloadStatus status = mDh.getDownloadStatus(uid);
            switch (status) {
                case STATUS_COMPLETE:
                    mStates[pos] = new State(100);
                    break;
                case STATUS_DOWNLOADING:
                    mStates[pos] = new State(mDh.getLastProgress(uid));
                    break;
                case STATUS_UNKNOWN:
                    if (fileExists(pos)) {
                        mStates[pos] = new State(100); // previously downloaded
                    } else {
                        mStates[pos] = new State(0);
                    }
                    break;

                default:
                    mStates[pos] = new State(0);
            }
        }
    }

    private boolean fileExists(int pos) {
        return new File(mDirLocation + mFileNames[pos]).exists();
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Object getItem(int position) {
        return mStates[position];
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    public void setProgressData(int position, int progress) {
        mStates[position].progress = progress;
    }

    public String getNameByUid(long uid) {
        return mFileNames[getPosByUid(uid)];
    }

    // onClick for start/cancel buttons
    final OnClickListener mBtnListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (Integer) v.getTag();
            switch (v.getId()) {
                case R.id.downloadBtn:
                    mDh.startDownload(mUids[pos], mUrls[pos], mDirLocation + mFileNames[pos]);
                    break;
                case R.id.cancelBtn:
                    mDh.cancelDownload(mUids[pos]);
                    mStates[pos].progress = 0;
                    notifyDataSetChanged(); // show 0 progress when cancel
                    break;
                case R.id.playBtn:
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse("file://" + mDirLocation + mFileNames[pos]), "video/*");
                    mContext.startActivity(intent);
                    break;
            }
        }
    };

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.thumbnail_list_item, null);
            holder = new ViewHolder();
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.videoPreview);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.progressBar);
            holder.download = (Button) convertView.findViewById(R.id.downloadBtn);
            holder.download.setOnClickListener(mBtnListener);
            holder.cancel = (Button) convertView.findViewById(R.id.cancelBtn);
            holder.cancel.setOnClickListener(mBtnListener);
            holder.play = (Button) convertView.findViewById(R.id.playBtn);
            holder.play.setOnClickListener(mBtnListener);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.download.setTag(position);
        holder.cancel.setTag(position);
        holder.play.setTag(position);
        // holder.thumbnail.setImageResource(mThumbIds[position]);
        holder.thumbnail.setImageBitmap(mBitmaps[position]);

        // update view state
        int progress = mStates[position].progress;
        holder.progress.setProgress(progress);
        // this is probably very dumb way of checking progress every time - FIXME
        if (progress == 100) { // downloaded, show clickabe "play" overlay
            holder.play.setVisibility(View.VISIBLE);
        } else {
            holder.play.setVisibility(View.INVISIBLE);
        }

        // if (progress == -1) {
        // // set pending
        // holder.progress.setIndeterminate(true);
        // } else if (progress == 100) {
        // // download complete
        // holder.cancel.setText("delete"); // change cancel button to delete
        // holder.download.setEnabled(false); // disable download button
        // // holder.progress.setEnabled(false); // disable progress bar
        // holder.progress.setVisibility(View.INVISIBLE);
        // } else {
        // holder.cancel.setText("cancel");
        // holder.download.setEnabled(true);
        // // holder.progress.setEnabled(true);
        // holder.progress.setVisibility(View.VISIBLE);
        // holder.progress.setProgress(progress);
        // }

        return convertView;
    }

    static class ViewHolder {
        ImageView thumbnail;
        Button download, cancel, play;
        ProgressBar progress;
    }

    int getPosByUid(long uid) {
        return (int) (uid - 1000L); // we have chosen "easy" uids
    }

    // this should be called off UI thread
    private void initBitmaps() {
        for (int i = 0; i < mBitmaps.length; ++i) {
            mBitmaps[i] = BitmapFactory.decodeResource(mContext.getResources(), mThumbIds[i]);
        }

    }

    private static class State {
        State(int p) {
            progress = p;
        }
        int progress = 0; // 0 - unknown progress
                              // -1 - pending (queued for download)
                              // 100 - downloaded(stored)
    }


    /////////////////////////////////////////////////////////////////////////////////////////
    // Hardcoded static thumbnails with urls
    /////////////////////////////////////////////////////////////////////////////////////////
    
    private static int NUM_ITEMS = 8;

    // hardcoded thumbnails - only for testing purposes
    private Integer[] mThumbIds = {
            R.drawable.sample_0,
            R.drawable.sample_1,
            R.drawable.sample_2,
            R.drawable.sample_3,
            R.drawable.sample_4,
            R.drawable.sample_5,
            R.drawable.sample_6,
            R.drawable.sample_7,
    };

    private Long[] mUids = {
            1000L,
            1001L,
            1002L,
            1003L,
            1004L,
            1005L,
            1006L,
            1007L,
    };

    private String[] mUrls = {
            "http://ak.picdn.net/footage/assets/montage/montage.mp4",
            "http://ak4.picdn.net/shutterstock/videos/4171204/preview/stock-footage--s-the-history-of-nuclear-bomb-testing-is-shown.mp4",
            "http://ak.picdn.net/shutterstock/videos/3379220/preview/stock-footage-woman-doing-yoga-exercise-called-makarasana-or-the-crocodile-pose.mp4",
            "http://ak7.picdn.net/shutterstock/videos/2137973/preview/stock-footage-dolly-salmon-steak-and-red-caviar.mp4",
            "http://ak1.picdn.net/shutterstock/videos/2823850/preview/stock-footage-slow-motion-of-professional-swimmer-swimming-the-crawl-in-outdoor-pool.mp4",
            "http://ak6.picdn.net/shutterstock/videos/2266346/preview/stock-footage-berlin-skyline-city-timelapse-with-cloud-dynamic-in-full-hd-p-german-capital.mp4",
            "http://ak2.picdn.net/shutterstock/videos/2907850/preview/stock-footage-holiday-fireworks-happy-new-year-text-animation-series-version-from-to-thing.mp4",
            "http://ak2.picdn.net/shutterstock/videos/4699241/preview/stock-footage-pov-surfing-slow-motion.mp4",
    };

    private String[] mFileNames = {
            "video1.mp4",
            "video2.mp4",
            "video3.mp4",
            "video4.mp4",
            "video5.mp4",
            "video6.mp4",
            "video7.mp4",
            "video8.mp4",
    };
}
