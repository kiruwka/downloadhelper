**downloadHelper** is generic and reusable solution to facilitate downloads.

Please build *downloadHelper* project in this repo to produce a library .jar file.
If you use eclipse (like myself) you could tick *"isLibrary"* checkbox in android project configuration.
 
It is declared to support API starting with version 8, but in fact could be re-compiled to support earlier versions too (not tested).

Important note : if you plan to support API >= 9, please consider using android [DownloadManager](http://developer.android.com/reference/android/app/DownloadManager.html) instead.

## Here are the notes on how to use this library with your application:

* Include the following in your app�s manifest:

Use permissions:

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

And service :

    <service android:name="com.kiruwka.android.download.DownloadService" />


* Set your project reference downloadHelper library.




#### Here is the list of API exposed by downloadHelper:

  * DownloadHelper getInstance(Context context) - to get a singleton instance

  * startDownload(long uid, String url, String filePath) - starts download

  * cancelDownload(long uid) - cancels ongoing or pending download

Downloaded resource is mainly identified by its unique uid.

  * registerOnErrorListener, registerOnProgressListener, registerOnCompleteListener - register listeners to receive download events

  * unregisterAllListeners() - unregister listeners
It is recommended to call this method in Activity.onPause (or onDestroy) to avoid memory leaks.
Registering listeners could be done in Activity.onResume/onCreate.

Sometimes, UI (Activity) can miss download events due to its lifetime cycle changes during ongoind download (for example when unregistering listeners as explained above).
In that case the following set of API to query download information for given download might be useful:

  * getDownloadStatus(long uid), getLastProgress(long uid), getLastError(long uid).

getDownloadStatus will only return information about ongoing/pending/finished_with_error downloads. 
For already downloaded files (when returned downloadStatus is STATUS_UNKNOWN)
UI will have to check with separate ResourceStore component for downloaded file availability.



# Note : 
this repository also contains **downloadHelperInAction** project, which is a sample and very basic application demonstrating capabilities of downloadHelper.



## Some assumptions related to downloadHelper implementation :

- All provided downloadHelper API is called from UI thread. It is not guaranteed to be race-condition free otherwise.

- It is not required to run download Service in a separate process. If needed it could be easily configured to do so otherwise.


## Some core features of downloadHelper

- Parallel downloads (currently hardcoded to 2 threads, but will be configurable in future)

- Start/stop functionality with feedback (callbacks) returned on UI thread

- Messaging between UI and Service is done via intents. Intents reported by Service are received by Local (in-process) broadcast receiver.

- Download notifications (very simple)

- Network disconnection management (affected downloads are re-started when network is back)

- Some consistency preserved using temp download files.

- Service lifetime is very strict � it only lives as long as it is physically downloading data. Otherwise it is stopped.


## Improvements, TODOs :

- Configurable number of parallel downloads setting

- Configurable progress update intervals (currently fixed to half second)

- Pause/resume downloads, re-using temp files and �Range� request to server

- Persistence of download progress/queue across application restart by using extra storage

- Add support to https streams with configurable authentication options