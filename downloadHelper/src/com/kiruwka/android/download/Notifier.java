package com.kiruwka.android.download;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * static class to facilitate showing notifications
 * 
 * @author km
 */
// FIXME duplicated code for Builder, refactor later
public class Notifier {
    private static final int NOTIFICATION_ID = 123456;

    public static void showProgressNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.stat_sys_download)
                        .setContentTitle("downloadHelper")
                        .setContentText("downloading")
                        .setTicker("downloading")
                        .setOngoing(true);
        PendingIntent contentIntent = PendingIntent.getActivity(
                context.getApplicationContext(),
                0,
                new Intent(),
                PendingIntent.FLAG_UPDATE_CURRENT); // no-op intent
        mBuilder.setContentIntent(contentIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static void showCompleteNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.stat_sys_download_done)
                        .setContentTitle("downloadHelper")
                        .setContentText("done : no downloads left");
        PendingIntent contentIntent = PendingIntent.getActivity(
                context.getApplicationContext(),
                0,
                new Intent(),
                PendingIntent.FLAG_UPDATE_CURRENT); // no-op intent
        mBuilder.setContentIntent(contentIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static void showNetworkUnavailableNotification(Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.stat_sys_download_done)
                        .setContentTitle("downloadHelper")
                        .setContentText("NETWORK IS UNAVAILABLE");
        PendingIntent contentIntent = PendingIntent.getActivity(
                context.getApplicationContext(),
                0,
                new Intent(),
                PendingIntent.FLAG_UPDATE_CURRENT); // no-op intent
        mBuilder.setContentIntent(contentIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}
