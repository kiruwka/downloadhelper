package com.kiruwka.android.download;

import android.os.Parcel;
import android.os.Parcelable;

// wrapper namespace for two message types : Ui to Service messages and Service to UI messages
public class Messages {
    public static final String COMMAND = "UiToService";
    public static final String CALLBACK = "ServiceToUi";

    /**
     * structure-like message dispatched to DownloadService
     * 
     * @author user_km
     */
    static class CommandMessage implements Parcelable {
        public static final int START = 1; // start download
        public static final int CANCEL = 2; // cancel download FIXME enums and Parcebles ?

        int command;
        long uid;
        String url;
        String path;

        CommandMessage() {}

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            out.writeInt(command);
            out.writeLong(uid);
            out.writeString(url);
            out.writeString(path);
        }

        /** Static field used to regenerate object, individually or as arrays */
        public static final Parcelable.Creator<CommandMessage> CREATOR = new Parcelable.Creator<CommandMessage>() {
            public CommandMessage createFromParcel(Parcel in) {
                return new CommandMessage(in);
            }

            public CommandMessage[] newArray(int size) {
                return new CommandMessage[size];
            }
        };

        /** Ctor from Parcel, reads back fields IN THE ORDER they were written */
        public CommandMessage(Parcel in) {
            command = in.readInt();
            uid = in.readLong();
            url = in.readString();
            path = in.readString();
        }
    }

    /**
     * message sent by Service to Activity (thru DownloadHelper BroadcastReceiver)
     * 
     * @author user_km
     */
    static class CallbackMessage implements Parcelable {
        public static final int PROGRESS_UPDATE = 1;
        public static final int ERROR = 2;
        public static final int COMPLETE = 3;

        int event;
        long uid;
        int progress;
        String msg;

        CallbackMessage() {}

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            out.writeInt(event);
            out.writeLong(uid);
            out.writeInt(progress);
            out.writeString(msg);
        }

        /** Static field used to regenerate object, individually or as arrays */
        public static final Parcelable.Creator<CallbackMessage> CREATOR = new Parcelable.Creator<CallbackMessage>() {
            public CallbackMessage createFromParcel(Parcel in) {
                return new CallbackMessage(in);
            }

            public CallbackMessage[] newArray(int size) {
                return new CallbackMessage[size];
            }
        };

        /** Ctor from Parcel, reads back fields IN THE ORDER they were written */
        public CallbackMessage(Parcel in) {
            event = in.readInt();
            uid = in.readLong();
            progress = in.readInt();
            msg = in.readString();
        }
    }
}
