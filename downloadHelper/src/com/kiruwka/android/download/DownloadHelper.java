package com.kiruwka.android.download;

import java.util.concurrent.ConcurrentHashMap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.kiruwka.android.download.Messages.CallbackMessage;
import com.kiruwka.android.download.Messages.CommandMessage;

/**
 * Singleton
 * Wrapper of our Download functionality requires application context to work with service
 * 
 * @author user_km
 */
public class DownloadHelper {
    private static final String TAG = DownloadHelper.class.getSimpleName();
    private static DownloadHelper sInstance = null;

    private Context mContext = null;
    private OnErrorListener mOnErrorListener;
    private OnCompleteListener mOnCompleteListener;
    private OnProgressListener mOnProgressListener;
    private DownloadEventReceiver mDownloadEventReceiver = null;
    private CommandMessage mCommand = new CommandMessage(); // messages to service
    // handling network interruptions
    public boolean mNetworkAvailable = false;

    // keeps state of downloads based on download requests by UI
    // and last event reported by DownloadService for given uid not yet delivered to UI
    // this map is also used to manage persistent downloads accross connectivity changes
    private final ConcurrentHashMap<Long, DownloadEventInfo> mDownloads = new ConcurrentHashMap<Long, DownloadEventInfo>();

    public enum DownloadStatus {
        STATUS_UNKNOWN,
        STATUS_PENDING,
        STATUS_ERROR,
        STATUS_DOWNLOADING,
        STATUS_CANCELLED,
        STATUS_COMPLETE,
        LAST_STATUS,
    }

    private DownloadHelper(Context context) {
        // we only keep track of application context to avoid accidental leaks of activity
        // alternatively we could keep WeakReference to activity context
        mContext = context.getApplicationContext();
        IntentFilter filter = new IntentFilter(DownloadService.DOWNLOAD_ACTION);
        // TODO refactor a bit, since we don't actually use
        // mDownloadEventReciver elsewhere
        mDownloadEventReceiver = new DownloadEventReceiver();
        LocalBroadcastManager.getInstance(mContext)
                .registerReceiver(mDownloadEventReceiver, filter);
        mNetworkAvailable = isNetworkAvailable();
        mContext.registerReceiver(new ConnectivityReceiver(),
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /*
     * information about last download event associated with uid
     */
    private static class DownloadEventInfo {
        DownloadStatus status = DownloadStatus.STATUS_PENDING;
        String lastError = null;
        int lastProgress = 0;

        // these are needed when we re-download on networkConnected event
        String url, path;

        DownloadEventInfo(String url, String path) {
            this.url = url;
            this.path = path;
        }
    }

    public static DownloadHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DownloadHelper(context);
        }
        return sInstance;
    }

    /**
     * starts download
     * 
     * @param uid - unique identifier of the resource
     * @param url - url of the stream to download from
     * @param path - full path to file to be saved
     */
    public void startDownload(long uid, String url, String filePath) {
        DownloadEventInfo item = new DownloadEventInfo(url, filePath); // STATUS_PENDING
        mDownloads.put(uid, item);
        if (isNetworkAvailable()) { // start the service
            mCommand.command = CommandMessage.START;
            mCommand.path = filePath;
            mCommand.uid = uid;
            mCommand.url = url;
            Intent intent = new Intent(mContext, DownloadService.class).putExtra(Messages.COMMAND,
                    mCommand);
            mContext.startService(intent);
            Notifier.showProgressNotification(mContext);
        }
    }

    /**
     * cancels [pending or ongoing] download
     * 
     * @param uid - unique identifier of the resource if resource with provided uid is not being queued or downloaded ->
     *            no action performed
     */
    public void cancelDownload(long uid) {
        if (mDownloads.containsKey(uid)) {
            mDownloads.remove(uid);
            if (!hasPendingOrOngoingTasks()) {
                Notifier.showCompleteNotification(mContext);
            }

            // FIXME do this only if service is running
            mCommand.command = CommandMessage.CANCEL;
            mCommand.uid = uid;
            Intent intent = new Intent(mContext, DownloadService.class).putExtra(Messages.COMMAND,
                    mCommand);
            mContext.startService(intent);
        }
    }

    /*
     * set of APIs for UI to query download status when some callbacks were missed for example when Activity was
     * paused/destroyed during download
     */

    /**
     * API to get current download status if some callbacks were missed for example when Activity is paused/destroyed
     */
    // we dont explicitly synchronize here for performance, assuming this API
    // is only called from UI thread TODO: assert these assumptions
    public DownloadStatus getDownloadStatus(long uid) {
        DownloadStatus result = DownloadStatus.STATUS_UNKNOWN;
        DownloadEventInfo info = mDownloads.get(uid);
        if (info == null) {
            return result; // STATUS_UNKNOWN
        }

        result = info.status;
        return result;
    }

    /**
     * API to get current progress if some callbacks were missed for example when Activity is paused/destroyed
     */
    public int getLastProgress(long uid) {
        DownloadEventInfo info = mDownloads.get(uid);
        return info == null ? 0 : info.lastProgress;
    }

    /**
     * API to get last error if some callbacks were missed for example when Activity is paused/destroyed during ongoing
     * download
     */
    public String getLastError(long uid) {
        DownloadEventInfo info = mDownloads.get(uid);
        if (info == null || info.status != DownloadStatus.STATUS_ERROR) {
            return null; // no error
        }
        String result = info.lastError;
        // we can remove this item from hashMap after we informed the UI(Client)
        mDownloads.remove(uid);
        return result;
    }

    /**
     * should be called in Activity#onPause() to prevent activity leak
     */
    public void unregisterAllListeners() {
        mOnErrorListener = null;
        mOnProgressListener = null;
        mOnCompleteListener = null;
        // TODO: to be able to unregister receiver, all state information should
        // be saved in DownloadService.
        // LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mDownloadEventReceiver);
    }

    public void registerOnErrorListener(OnErrorListener arg) {
        mOnErrorListener = arg;
    }

    public void registerOnProgressListener(OnProgressListener arg) {
        mOnProgressListener = arg;
    }

    public void registerOnCompleteListener(OnCompleteListener arg) {
        mOnCompleteListener = arg;
    }

    public interface OnErrorListener {
        void onError(long uid, String msg);
    }

    // TODO support extra API with progress in bytes
    public interface OnProgressListener {
        void onProgressUpdate(long uid, int progress);
    }

    public interface OnCompleteListener {
        void onComplete(long uid);
    }

    // reporting error to UI if it listens to it, otherwise just remember it
    private void reportError(long uid, String msg) {
        DownloadEventInfo info = mDownloads.get(uid);
        if (info == null) {
            return; // has been cancelled already before
        }
        if (!isNetworkAvailable()) {
            Log.d(TAG, "no network, do not report error");
            info.status = DownloadStatus.STATUS_PENDING;
            // replace error with pending in case network is unavailable
            // because we are going to re-download this
            // when we will receive 'network available'
            // TODO potentially change with STATUS_PAUSED when resuming is
            // impelmented
            return;
        }

        // report to UI in case it listens
        if (mOnErrorListener != null) {
            mDownloads.remove(uid); // reported to UI
            mOnErrorListener.onError(uid, msg);
        } else { // otherwise remember it so UI can check later with
                 // getLastError()
            info.status = DownloadStatus.STATUS_ERROR;
            info.lastError = msg;
        }
    }

    private void reportProgress(long uid, int progress) {
        DownloadEventInfo info = mDownloads.get(uid);
        if (info == null) {
            return; // has been cancelled already before
        }
        info.status = DownloadStatus.STATUS_DOWNLOADING;
        info.lastProgress = progress;

        if (mOnProgressListener != null) {
            mOnProgressListener.onProgressUpdate(uid, progress);
        }
    }

    private void reportComplete(long uid) {
        mDownloads.remove(uid);
        if (!hasPendingOrOngoingTasks()) {
            Notifier.showCompleteNotification(mContext);
        }
        if (mOnCompleteListener != null) {
            // we notify onCompletion even if download was cancelled meanwhile.
            mOnCompleteListener.onComplete(uid);
        }

    }
    
    // check if we have ongoing or pending tasks
    private boolean hasPendingOrOngoingTasks() {
        for (DownloadEventInfo info : mDownloads.values()) {
            if (info.status == DownloadStatus.STATUS_PENDING
                    || info.status == DownloadStatus.STATUS_DOWNLOADING) {
                return true;
            }
        }
        return false;
    }

    /**
     * this class receives messages from DownloadService
     */
    class DownloadEventReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent callbackIntent) {
            CallbackMessage callbackData = callbackIntent.getParcelableExtra(Messages.CALLBACK);
            long uid = callbackData.uid;
            int event = callbackData.event;
            switch (event) {
                case CallbackMessage.PROGRESS_UPDATE:
                    int progress = callbackData.progress;
                    reportProgress(uid, progress);
                    break;
                case CallbackMessage.ERROR:
                    String msg = callbackData.msg;
                    Log.e(TAG, "error = " + msg);
                    reportError(uid, msg);
                    break;
                case CallbackMessage.COMPLETE:
                    reportComplete(uid);
                    break;
            }
        }
    }

    /**
     * listens for network connectivity changes and informs us
     *
     */
    class ConnectivityReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean noConnectivity = intent.getBooleanExtra(
                    ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
            Log.d(TAG, "received connectivity stat:" + noConnectivity);
            if (mNetworkAvailable == isNetworkAvailable()) { // no availability change, no action
                return;
            }

            if (noConnectivity) {
                mNetworkAvailable = false;
                notifyDisconnected();
            } else {
                mNetworkAvailable = true;
                notifyConnected();
            }
        }
    }

    boolean isNetworkAvailable() {
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnected();
        return isConnected;
    }

    private void notifyConnected() {
        // iterate through hashmap, starting pending downloads
        Log.d(TAG, "network connected");
        mCommand.command = CommandMessage.START;
        DownloadEventInfo info;
        boolean restarted = false;
        // TODO: thread safety!
        for (long uid : mDownloads.keySet()) {
            if ((info = mDownloads.get(uid)).status == DownloadStatus.STATUS_PENDING) {
                mCommand.uid = uid;
                mCommand.path = info.path;
                mCommand.url = info.url;
                Intent intent = new Intent(mContext, DownloadService.class).putExtra(
                        Messages.COMMAND,
                        mCommand);
                mContext.startService(intent); // ask service to download this
                                               // entry
                restarted = true;
            }
        }
        if (restarted) {
            Notifier.showProgressNotification(mContext);
        }
    }

    public void notifyDisconnected() {
        Notifier.showNetworkUnavailableNotification(mContext);
    }

}
