package com.kiruwka.android.download;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import android.util.Log;

/**
 * fully static class to provide default customizattion for HttpUrlConnection.
 * 
 * @author user_km
 */
public class HttpConnectionFactory {
    private static final String TAG = HttpConnectionFactory.class.getSimpleName();
    private static final int TIMEOUT = 10/* sec */* 1000;

    /** create new connection for GET request */
    public static HttpURLConnection newGetRequest(String urlString) {
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT);
            connection.setReadTimeout(TIMEOUT);

            if (connection instanceof HttpsURLConnection) {
                // set additional https params, Trusted store, SSL params, etc
            }

            // connection.setInstanceFollowRedirects(true);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Url is malformed : " + urlString);
            return null;
        } catch (IOException e) {
            Log.e(TAG, "FATAL: Can not create connection for url : " + urlString);
            return null;
        }

        return connection;
    }
}
