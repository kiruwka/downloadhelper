package com.kiruwka.android.download;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.kiruwka.android.download.Messages.CallbackMessage;
import com.kiruwka.android.download.Messages.CommandMessage;

/**
 * started and not bound(so far) Service messaging from Activity->Service is via intents with Messages.CommandMessages
 * Service->Activity is via intens with Messages.CallbackMessages (via LocalBroadcastManager)
 * 
 * @author user_km
 */
public class DownloadService extends Service {
    private static final String TAG = DownloadService.class.getSimpleName();
    private CallbackMessage mEvent = new CallbackMessage(); // messages back to UI (thru DownloadHelper)

    // FIXME better remove final, to be configurable.
    private static final int THREAD_POOL_SIZE = 2; // number of parallel downloads
    private final ExecutorService mThreadPoolExecutor = Executors.newFixedThreadPool(THREAD_POOL_SIZE);

    public static final String DOWNLOAD_ACTION = "com.kiruwka.android.download.EVENT_ACTION";
    private final ConcurrentHashMap<Long, DownloadTask> mActiveTasks = new ConcurrentHashMap<Long, DownloadTask>();
    // restricting updating progress update too often
    private static final long PROGRESS_UPDATE_INTERVAL = 500; // ms
    private long mLastProgressUpdate = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return START_STICKY; // Run until explicitly stopped.
    }

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void handleCommand(Intent commandIntent) {
        assert (commandIntent.hasExtra(Messages.COMMAND));
        CommandMessage commandData = commandIntent.getParcelableExtra(Messages.COMMAND);
        Log.d(TAG, "service handling command " + commandData);
        long uid = commandData.uid;

        switch (commandData.command) {
            case CommandMessage.START:
                String url = commandData.url;
                String path = commandData.path;
                startDownloadIfNeeded(uid, url, path);
                break;
            case CommandMessage.CANCEL:
                cancelDownload(uid);
                break;
        }
    }

    private void cancelDownload(long uid) {
        DownloadTask task = mActiveTasks.get(uid);
        if (task != null) {
            task.cancel(true);
        }
    }

    private void startDownloadIfNeeded(long uid, String url, String path) {
        if (mActiveTasks.containsKey(uid)) {
            return; // we have this download either pending or running
        }
        // create and submit new download task
        DownloadTask task = new DownloadTask(uid, url, new File(path) /* TODO check for null path?*/);
        mActiveTasks.put(uid, task);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            // alas we don't have control over pool size on pre-honeycomb devices
            // TODO switch from Executor+AsyncTask to Executor+Runnable then ?
            task.execute();
        } else {
            // Call exists since API 11.
            task.executeOnExecutor(mThreadPoolExecutor);
        }

    }

    private void notifyOnError(long uid, String errorMsg) {
        Log.d(TAG, "notifyError " + errorMsg);
        mEvent.event = CallbackMessage.ERROR;
        mEvent.uid = uid;
        mEvent.msg = errorMsg;
        Intent intent = new Intent(DOWNLOAD_ACTION).putExtra(Messages.CALLBACK, mEvent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void notifyProgressUpdate(long uid, int progress) {
        if (System.currentTimeMillis() - mLastProgressUpdate < PROGRESS_UPDATE_INTERVAL) {
            return; // we don't want to bother UI too often
        }
        mLastProgressUpdate = System.currentTimeMillis();

        mEvent.event = CallbackMessage.PROGRESS_UPDATE;
        mEvent.uid = uid;
        mEvent.progress = progress;
        Intent intent = new Intent(DOWNLOAD_ACTION).putExtra(Messages.CALLBACK, mEvent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void notifyComplete(long uid) {
        mEvent.event = CallbackMessage.COMPLETE;
        mEvent.uid = uid;
        Intent intent = new Intent(DOWNLOAD_ACTION).putExtra(Messages.CALLBACK, mEvent);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * AsyncTask that will asynchronously download the file
     */
    class DownloadTask extends AsyncTask<Void, Void, Void> {
        private long mUid; // unique identification of the resource
        private String mUrl;
        private File mFile; // location to store file

        public DownloadTask(long uid, String url, File path) {
            mUid = uid;
            mUrl = url;
            mFile = path;
        }

        /**
         * download routine running in a separate thread
         */
        @Override
        protected Void doInBackground(Void... args) {
            // show notification
            HttpURLConnection connection = HttpConnectionFactory.newGetRequest(mUrl);
            if (connection == null) {
                Log.e(TAG, "can not create connection");
                notifyOnError(mUid, "Error establishing connection to url : " + mUrl);
            }

            InputStream is = null;
            OutputStream os = null;
            File tempFile = null; // to avoid having inconsistent media due to interruptions
            try {
                connection.connect();
                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    is = new BufferedInputStream(connection.getInputStream());
                    // check that mFile is writable in CheckBundle.
                    if (!makeDirs(mFile)) {
                        return null;
                    }
                    // makeTempFile for mFile
                    tempFile = tempFile(mFile, mUid);

                    // write to temp file
                    os = new BufferedOutputStream(new FileOutputStream(tempFile));

                    // FIXME: handle case when no content length available
                    int contentLength = connection.getContentLength();

                    byte data[] = new byte[8 * 1024];
                    long progressBytes = 0;
                    int count = 0;
                    while ((count = is.read(data)) != -1) { // downloading loop
                        if (isCancelled()) {
                            return null; // just stop the task as requested. No feedback needed
                        }
                        os.write(data, 0, count);
                        progressBytes += count;
                        notifyProgressUpdate(mUid, (int) (progressBytes * 100 / contentLength));
                    }
                    // move temp -> mFile
                    moveFile(tempFile, mFile);
                    notifyComplete(mUid);
                }
            } catch (IOException e) {
                e.printStackTrace();
                notifyOnError(mUid, e.getMessage());
            } finally {
                // remove temp file
                removeTempFile(tempFile);
                connection.disconnect();
                try {
                    if (os != null) {
                        os.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException ignore) {
                }

                mActiveTasks.remove(mUid);
                if (mActiveTasks.isEmpty()) { // no more tasks -> stop the service
                    stopSelf();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void arg) {
        }

        private File tempFile(File destinationPath, long uid) {
            File parentDir = destinationPath.getParentFile();
            return new File(parentDir, uid + ".tmp");
        }

        private void moveFile(File tempFile, File destinationPath) {
            if (tempFile != null && tempFile.isFile()) {
                tempFile.renameTo(destinationPath);
            }
        }

        private void removeTempFile(File tempFile) {
            if (tempFile != null && tempFile.isFile()) {
                tempFile.delete();
            }
        }

        private boolean makeDirs(File destinationPath) {
            File parentDir = null;
            if (destinationPath == null ||
                    (parentDir = destinationPath.getParentFile()) == null) {

                notifyOnError(mUid, "wrong destination file path");
                return false;
            }

            // Create the storage directory if it does not exist
            if (!parentDir.exists()) {
                if (!parentDir.mkdirs()) {
                    Log.e("TAG", "failed to create directory " + parentDir.getAbsolutePath());
                    notifyOnError(mUid, "failed to create directory " + parentDir.getAbsolutePath());
                    return false;
                }
            }
            return true;
        }

    } // DownloadTask

} // DownloadService
